package io.swagger;


import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import io.swagger.api.DefaultApi;
import io.swagger.api.impl.DefaultApiServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
@EnableAutoConfiguration
//@ComponentScan(basePackageClasses = DefaultApi.class)
@ComponentScan(basePackages = "io.swagger.api")
public class AppConfig {

	@Autowired private DefaultApiServiceImpl defaultApi;

	@Bean(destroyMethod = "shutdown")
	public SpringBus cxf() {
		return new SpringBus();
	}

	@Bean(destroyMethod = "destroy") @DependsOn("cxf")
	public Server jaxRsServer() {
		final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
		List providers= new ArrayList<>();
		providers.add(new JacksonJsonProvider());		
		providers.add(new ApiOriginFilter());		
		factory.setServiceBean(defaultApi);
		factory.setProviders(providers);
		factory.setBus(cxf());
		factory.setAddress("/");
        factory.setFeatures(Arrays.asList(new Swagger2Feature()));

		return factory.create();
	}

	@Bean
	public ServletRegistrationBean cxfServlet() {
		final ServletRegistrationBean servletRegistrationBean = 
			new ServletRegistrationBean(new CXFServlet(), "/api/*");
		servletRegistrationBean.setLoadOnStartup(1);
		return servletRegistrationBean;
	}
}
