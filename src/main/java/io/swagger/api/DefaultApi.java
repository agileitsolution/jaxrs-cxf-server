package io.swagger.api;

import io.swagger.model.ErrorModel;
import io.swagger.model.NewPet;
import io.swagger.model.Pet;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.ext.multipart.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.jaxrs.PATCH;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/pets")
//@Api(value = "/", description = "")
@Service
public interface DefaultApi  {

    @POST
//    @Path("/pets")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    public Pet addPet(@Valid NewPet pet);

//    @DELETE
//    @Path("/{id}")
//    @Consumes({ "application/json" })
//    @Produces({ "application/json" })
//    @ApiOperation(value = "", tags={  })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 204, message = "pet deleted"),
//        @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class) })
//    public void deletePet(@PathParam("id") Long id);

    @GET
    @Path("/{id}")
//    @Consumes({ "application/json" })
    @Produces({ "application/json", "application/xml", "text/xml", "text/html" })
//    @ApiOperation(value = "", tags={  })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 200, message = "pet response", response = Pet.class),
//        @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class) })
    public Pet findPetById(@PathParam("id") Long id);

    @GET
//    @Path("/pets")
//    @Consumes({ "application/json" })
    @Produces({ "application/json", "application/xml", "text/xml", "text/html" })
//    @ApiOperation(value = "", tags={  })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 200, message = "pet response", response = Pet.class, responseContainer = "List"),
//        @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class, responseContainer = "List") })
    public List<Pet> findPets(@QueryParam("tags") List<String> tags, @QueryParam("limit") Integer limit);
}

