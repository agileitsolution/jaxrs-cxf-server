package io.swagger.api.impl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.*;
import io.swagger.model.ErrorModel;
import io.swagger.model.NewPet;
import io.swagger.model.Pet;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

import org.apache.cxf.jaxrs.ext.multipart.*;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Component;

//@Path("/")
@Api(value = "/", description = "")
@Component
public class DefaultApiServiceImpl implements DefaultApi {

    @ApiOperation(value = "", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "pet response", response = Pet.class),
        @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class) })
    public Pet addPet(NewPet pet) {
    	Pet pet1 = buildPet( "bobi","tag1", 11l);        
        return pet1;
    }
    
    public void deletePet(Long id) {
        // TODO: Implement...
        
        
    }
//    @GET
//    @Path("/pets/{id}")
//    @Consumes({ "application/json" })
//    @Produces({ "application/json", "application/xml", "text/xml", "text/html" })
    @ApiOperation(value = "", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "pet response", response = Pet.class),
        @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class) })
    public Pet findPetById(Long id) {
    	Pet pet = buildPet( "bobi","tag1", 11l);        
        return pet;
    }

//    @GET
//    @Path("/pets")
//    @Consumes({ "application/json" })
//    @Produces({ "application/json", "application/xml", "text/xml", "text/html" })
    @ApiOperation(value = "", tags={  })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "pet response", response = Pet.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "unexpected error", response = ErrorModel.class, responseContainer = "List") })
    public List<Pet> findPets(List<String> tags, Integer limit) {
        // TODO: Implement...
    	List<Pet> pets = new ArrayList<Pet>();
    	Pet pet = buildPet( "bobi","tag1", 11l);
    	pets.add(pet);
    	pet = buildPet( "jaky","tag1", 12l);
    	pets.add(pet);
    	pet = buildPet( "dzedi","tag1", 13l);
    	pets.add(pet);
        return pets;
    }

	private Pet buildPet(String name, String tag, Long id) {
		Pet pet = new Pet();
    	pet.setId(id);
    	pet.setName(name);
    	pet.setTag(tag);
		return pet;
	}
    
}

