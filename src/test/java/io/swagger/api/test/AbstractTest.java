package io.swagger.api.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.Before;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import io.swagger.api.DefaultApi;

public abstract class AbstractTest<R, T, F> {

    public DefaultApi api;

    @Before
    public void setup() {
        JacksonJsonProvider provider = new JacksonJsonProvider();
        List providers = new ArrayList();
        providers.add(provider);        
//        api = JAXRSClientFactory.create("http://petstore.swagger.io/api", DefaultApi.class, providers);
        api = JAXRSClientFactory.create("http://localhost:8080/api", DefaultApi.class, providers);
        org.apache.cxf.jaxrs.client.Client client = WebClient.client(api);        
        ClientConfiguration config = WebClient.getConfig(client); 
    }
    
	public abstract R buildRequest(Object ... args);
	public abstract T process(R request);
	public abstract void assertResult(Object ... args);
	
}
