/**
 * Swagger Petstore (Simple)
 * A sample API that uses a petstore as an example to demonstrate features in the swagger-2.0 specification
 *
 * OpenAPI spec version: 1.0.0
 * Contact: foo@example.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.api;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;

import io.swagger.api.test.AbstractTest;
import io.swagger.model.NewPet;
import io.swagger.model.Pet;

/**
 * API tests for DefaultApi
 */
public class DefaultApiTest extends AbstractTest<NewPet,Pet,Pet> {

    /**
     * 
     *
     * Creates a new pet in the store.  Duplicates are allowed
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addPetTest() {
        NewPet pet = this.buildRequest(11l, "name", "tag");
        Pet response = api.addPet(pet);
        this.assertResult(response);
    }
    
    /**
     * 
     *
     * deletes a single pet based on the ID supplied
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deletePetTest() {
        Long id = null;
        //api.deletePet(id);
        // TODO: test validations
    }
    
    /**
     * 
     *
     * Returns a user based on a single ID, if the user does not have access to the pet
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findPetByIdTest() {
        Long id = 11L;
        Pet response = api.findPetById(id);
        assertNotNull(response);
        // TODO: test validations
    }
    
    /**
     * 
     *
     * Returns all pets from the system that the user has access to
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void findPetsTest() {
        List<String> tags = null;
        Integer limit = null;
        //List<Pet> response = api.findPets(tags, limit);
        //assertNotNull(response);
        // TODO: test validations
    }

	@Override
	public NewPet buildRequest(Object ...args) {
		NewPet pet = new NewPet();
		pet.setId((Long) args[0]);
		pet.setName((String)args[1]);
		pet.setTag((String)args[2]);
		return pet;
	}

	@Override
	public Pet process(NewPet request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void assertResult(Object... args) {
		assertNotNull(args[0]);
		Pet pet = (Pet) args[0];
//		assertEquals(11l, pet.getId());
		assertEquals("bobi", pet.getName());
		assertEquals("tag1", pet.getTag());
	}
    
}
